const isEmpty = (string) => {
    if (string.toString().trim() === '') return true;
    else return false;
};

const isEmail = (email) => {
    const regEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    
    if (email.match(regEx)) return true;
    else return false;
};


const Validator = {

    validateSignupData: (data) => {
        let errors = {};
    
        if (isEmpty(data.email)) errors.email = 'Must not be empty.';
        else if (!isEmail(data.email)) errors.email = 'Must be a vaild email address.';
    
        if (isEmpty(data.password)) errors.password = 'Must not be empty.';
        if (data.password !== data.confirmPassword) errors.confirmPassword = 'Passwords must match';
        if (isEmpty(data.username)) errors.username = 'Must not be empty';
    
        return {
            errors,
            valid: Object.keys(errors).length === 0 ? true : false
        };
    },

    validateLoginData: (data) => {
        let errors = {};
    
        if (isEmpty(data.email)) errors.email = 'Must not be empty.';
        else if (!isEmail(data.email)) errors.email = 'Must be a vaild email address.';
    
        if (isEmpty(data.password)) errors.password = 'Must not be empty.';
        
        return {
            errors,
            valid: Object.keys(errors).length === 0 ? true : false
        };
    },

    reduceUserDetails: (data) => {
        let userDetails = {};
    
        if (!isEmpty(data.company.trim())) userDetails.company = data.company;
        if (!isEmpty(data.firstName.trim())) userDetails.firstName = data.firstName;
        if (!isEmpty(data.lastName.trim())) userDetails.lastName = data.lastName;
        if (!isEmpty(data.address.trim())) userDetails.address = data.address;
        if (!isEmpty(data.city.trim())) userDetails.city = data.city;
        if (!isEmpty(data.country.trim())) userDetails.country = data.country;
        if (!isEmpty(data.postalCode.trim())) userDetails.postalCode = data.postalCode;
        if (!isEmpty(data.city.trim())) userDetails.city = data.city;
        if (!isEmpty(data.description.trim())) userDetails.description = data.description;
        
        if (!isEmpty(data.facebook.trim())) {
            if (data.facebook.trim().substring(0, 4) !== 'http') {
                userDetails.facebook = `http://${ data.facebook.trim() }`
            } else userDetails.facebook = data.facebook;
        }
    
        if (!isEmpty(data.twitter.trim())) {
            if (data.twitter.trim().substring(0, 4) !== 'http') {
                userDetails.twitter = `http://${ data.twitter.trim() }`
            } else userDetails.twitter = data.twitter;
        }
    
        if (!isEmpty(data.linkedin.trim())) {
            if (data.linkedin.trim().substring(0, 4) !== 'http') {
                userDetails.linkedin = `http://${ data.linkedin.trim() }`
            } else userDetails.linkedin = data.linkedin;
        }
    
        return userDetails;
    },

    reduceAccountDetails: (data) => {
        if (!data.category || !data.category.length || isEmpty(data.category)) 
            data.category = [{ display: 'default', value: 'default' }];

        if (!data.money || isEmpty(data.money)) data.money = 0;

        if (!data.date || !Object.keys(data.date).length || isEmpty(data.date)) 
            data.date = {
                day: new Date().getDate(),
                month: new Date().getMonth(),
                year: new Date().getFullYear()
            };

        return data;
    },

    validateAccountCreate: (data) => {
        let errors = {};

        if (isEmpty(data.type)) errors.type = 'Must not be empty.';
        if (isEmpty(data.category)) errors.category = 'Must not be empty.';
        if (isEmpty(data.money)) errors.money = 'Must not be empty.';
        if (isEmpty(data.date)) errors.date = 'Must not be empty.';

        return {
            errors,
            valid: Object.keys(errors).length === 0 ? true : false
        };
    },

    reduceJobDetails: (data) => {
        if (!data.category || !data.category.length || isEmpty(data.category)) 
            data.category = [{ display: 'default', value: 'default' }];

        if (!data.status || isEmpty(data.status)) data.status = 1;

        return data;
    },

    validateJobCreate: (data) => {
        let errors = {};

        if (isEmpty(data.type)) errors.type = 'Must not be empty.';
        if (isEmpty(data.day)) errors.day = 'Must not be empty.';
        if (isEmpty(data.money) || data.money <= 0) errors.money = 'Must not be 0.';
        if (isEmpty(data.category)) errors.category = 'Must not be empty.';
        if (isEmpty(data.status)) errors.status = 'Must not be empty.';

        return {
            errors,
            valid: Object.keys(errors).length === 0 ? true : false
        };
    },

    reduceTodoDetails: (data) => {
        if (!data.list || !data.list.length || isEmpty(data.list)) 
            data.list = [];
        if (!data.done || isEmpty(data.done)) data.done = false;

        return data;
    },

    validateTodoCreate: (data) => {
        let errors = {};

        if (isEmpty(data.name)) errors.name = 'Must not be empty.';

        return {
            errors,
            valid: Object.keys(errors).length === 0 ? true : false
        };
    },

    reduceProjectDetails: (data) => {
        if (!data.status || isEmpty(data.status)) data.status = 'Open';
        if (!data.priority || isEmpty(data.priority)) data.priority = 'Normal';
        if (!data.active || isEmpty(data.active)) data.active = false;
        if (!data.checkIn || isEmpty(data.checkIn)) data.checkIn = '';

        return data;
    },

    validateProjectCreate: (data) => {
        let errors = {};

        if (isEmpty(data.name)) errors.name = 'Must not be empty.';
        if (isEmpty(data.estimate)) errors.estimate = 'Must not be empty.';

        return {
            errors,
            valid: Object.keys(errors).length === 0 ? true : false
        };
    },

    validateTeamCreate: (data) => {
        let errors = {};

        if (isEmpty(data.name)) errors.name = 'Must not be empty.';

        return {
            errors,
            valid: Object.keys(errors).length === 0 ? true : false
        };
    },

}

module.exports = Validator;