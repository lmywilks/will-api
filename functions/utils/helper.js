const { db } = require('../utils/admin');

const Helper = {
    UserDelete: async(snapshot, context) => {
        try {
            const username = context.params.username;
            const batch = db.batch();

            const settings = await db
                .collection('settings')
                .where('user', '==', username)
                .get();
            
            settings.forEach(doc => {
                batch.delete(db.doc(`/settings/${ doc.id }`));
            });

            const accounts = await db
                .collection('accounts')
                .where('user', '==', username)
                .get();
            
            accounts.forEach(doc => {
                batch.delete(db.doc(`/accounts/${ doc.id }`));
            });

            const jobs = await db
                .collection('jobs')
                .where('user', '==', username)
                .get();

            jobs.forEach(doc => {
                batch.delete(db.doc(`/jobs/${ doc.id }`));
            });

            const todo = await db
                .collection('todo')
                .where('user', '==', username)
                .get();

            todo.forEach(doc => {
                batch.delete(db.doc(`/todo/${ doc.id }`));
            });

            const restaurants = await db
                .collection('restaurants')
                .where('user', '==', username)
                .get();

            restaurants.forEach(doc => {
                batch.delete(db.doc(`/restaurants/${ doc.id }`));
            });

            const comments = await db
                .collection('comments')
                .where('user', '==', username)
                .get();

            comments.forEach(doc => {
                batch.delete(db.doc(`/comments/${ doc.id }`));
            });

            const teams = await db
                .collection('teams')
                .where('user', '==', username)
                .get();

            teams.forEach(doc => {
                batch.delete(db.doc(`/teams/${ doc.id }`));
            });

            let projectIds = [];

            const projects = await db
                .collection('projects')
                .where('user', '==', username)
                .get();

            projects.forEach(doc => {
                projectIds.push(doc.id);
                batch.delete(db.doc(`/projects/${ doc.id }`));
            });

            const tasks = await db
                .collection('tasks')
                .where('projectId', 'array-contains', projectIds)
                .get();

            tasks.forEach(doc => {
                batch.delete(db.doc(`/tasks/${ doc.id }`));
            });

            const friends = await db
                .collection('friends')
                .where('user', '==', username)
                .get();

            friends.forEach(doc => {
                batch.delete(db.doc(`/friends/${ doc.id}`));
            });

            return batch.commit();
        } catch (err) {
            console.log(err)
        }
    },
    RestaurantDelete: async (snapshot, context) => {
        try {
            const restaurantId = context.params.restaurantId;
            const batch = db.batch();

            const comments = await db
                .collection('comments')
                .where('restaurant', '==', restaurantId)
                .get();
            
            comments.forEach(doc => {
                batch.delete(db.doc(`/comments/${ doc.id }`));
            });

            return batch.commit();
        } catch (err) {
            console.log(err)
        }
    },
    TeamDelete: async (snapshot, context) => {
        try {
            const teamId = context.params.teamId;
            const batch = db.batch();

            const projects = await db
                .collection('projects')
                .where('team', '==', teamId)
                .get();

            projects.forEach(doc => {
                batch.delete(db.doc(`/projects/${ doc.id }`));
            });

            const tasks = await db
                .collection('tasks')
                .where('team', '==', teamId)
                .get();

            tasks.forEach(doc => {
                batch.delete(db.doc(`/tasks/${ doc.id }`));
            });

            return batch.commit();
        } catch (err) {
            console.log(err);
        }
    },
    ProjectDelete: async (snapshot, context) => {
        try {
            const projectId = context.params.projectId;
            const batch = db.batch();

            const tasks = await db
                .collection('tasks')
                .where('project', '==', projectId)
                .get();

            tasks.forEach(doc => {
                batch.delete(db.doc(`/tasks/${ doc.id }`));
            });

            return batch.commit();
        } catch (err) {
            console.log(err);
        }
    }
};

module.exports = Helper;