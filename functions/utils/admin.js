const admin = require('firebase-admin');
const { SERVICE_ACCOUNT } = require('./config');

admin.initializeApp({
    credential: admin.credential.cert(SERVICE_ACCOUNT),
    databaseURL: "https://will-play-app.firebaseio.com",
    storageBucket: "will-play-app.appspot.com"
});
const db = admin.firestore();

module.exports = { admin, db };