exports.FIREBASE = {
    apiKey: "AIzaSyDr09SoBLru8QpZslRHd3bKFurCiXs1l98",
    authDomain: "will-play-app.firebaseapp.com",
    databaseURL: "https://will-play-app.firebaseio.com",
    projectId: "will-play-app",
    storageBucket: "will-play-app.appspot.com",
    messagingSenderId: "577001707495",
    appId: "1:577001707495:web:ed36ff077580c458"
};

exports.SERVICE_ACCOUNT = {
    "type": "service_account",
    "project_id": "will-play-app",
    "private_key_id": "99d66a04a0984959f48357bdb788dfaba6609636",
    "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDCNTw28F6+jtCo\n9ezVlZXxeFytCvcfBoz+J8q6TGx5Buz5KAwOF0nzNdBU/r7Xaw85oGeqPH467b86\n46NxdHdIvoPUf8WKukjBMt1eKvgdP7TnPaTnUZIngWljhQclJgKQV2n25GGiHn+c\nh6IN9VUeLNxMOnXJbfanRdub1lX9/r8c5QdKSSuAJX+ucX/2e8s/bChhLZyODCt9\nCbkPllIOzId/c1QqB6j2H3VY9WakuQEZMwgokGPF1OuCCmrUL5Jqt64Xyhk2eGJn\n9sBE+s4/wpo/5O6G1HXmoVGZaFfPvUebgpcidqpgLDS5Zj5wIiI+cmnlCtMjccPX\nK/SiYIYTAgMBAAECggEAC83T0i6+HcOtJqRyPFX9t1ag2bGxtjkeEEzgt+un8i3O\nFaOk2aWMTJ0ezLzdq6AXbuHg8gEk1rXyxc2dP6Gb707kh/QGLBU27P2HiRg3E7m/\nw3YOFA6qZkeXb50EXvJNRk4/kXxI1yyEYdRvMKHbz1w3Yi+WBi0AyJBox8WLplkw\n5j/qBjXhYH7X3+YRvF/258+W6PLZ+PEdVAdabR12So/J4Z+p2C+TbmACbhZWqhtj\n3odAy8T19O0pc9sbv2yvuQfD7R19AiC8O6fZOsyb+FTi/ke6HY9BKYxqpsfera+a\n2aZnIELRefJ1e40mJk0KhPv/+Vs/5WFD1M4e+HRZ0QKBgQDf2icgnmuLomK6Uuty\nbJizLeNaoVYIWCXv1upOyl1/ttQu/ddSNfeqc76HCQAgYL06rJUF73NOr+iqMROP\nNBV7kC5TZ0uf3+o8q6yiASi0UO8vUE4RfLtXsTkaZX1emiqkLSbc4jUa4JxR2KeW\n+kRFb0DCkzWHx07FBd8k57UWowKBgQDeGTkoUljsz72wObyCx5uOOYqJ24wpYRhg\nWHeDzOSdS0D4t36nOzPSAK+uID2cD7sIJJfYgMArV9qFQUPuSdKT5kVMQVu2DdzD\nub26lyiIZzeSRBiQ+8eI3lHXojjo1M37gJppbcZXEEqzys2AMj2Rd8xZD3VH8Nms\n9/I4/DJ50QKBgCGvbE/mRvy+NOkg7TlMyjW8ZAgRWybqTNsolh3BKgy78HYZ0LsY\nT+qgfD1Jx3EX3aHkspAwFkPzCKoDtJPKHKl5at82HhUy1sbGdrXyQrPPsg6GoRjU\nkkSB8LveJd2EiYInIT//8knXHu7aEW4QvxFLyoz9JgMGpP3VAl1FWOTNAoGAEMWM\n2GunhXKGm4kwoBuctpfHy3NF2VYYtiXYGJ7fYLVvGZkZul0xI5mjqp2KqX9Sm2HQ\nCaxnVp6SxN9R1uvCOCL6drC00UGouysKlc2WZP6OZsA3rrMktUQ8n/EDJSn0yBpF\nDtMYWDjFu2AENrsUhSiryMnvDFi2x2YN55m74hECgYEAhJtHZxTkAEUPLMDX32Fu\nTvIr8f/v0h/FyVCjmFHrvUViXWt6/kqap0Oe7gsixuX7vRJhoFx/oY9/EmEim8Dy\nIKGJAetkDL+lZPIlwv1AGa8YnOt9eZ2JO7kel3rIAaV2J0p/Z3if42fDTPY1n/8d\nhHxLkR1x+difU6irhSSEhdc=\n-----END PRIVATE KEY-----\n",
    "client_email": "firebase-adminsdk-6gax0@will-play-app.iam.gserviceaccount.com",
    "client_id": "112729716553220984463",
    "auth_uri": "https://accounts.google.com/o/oauth2/auth",
    "token_uri": "https://oauth2.googleapis.com/token",
    "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
    "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-6gax0%40will-play-app.iam.gserviceaccount.com"
};

exports.FIREBASE_BUCKET_ROOT = `https://firebasestorage.googleapis.com/v0/b/will-play-app.appspot.com/o`;
  