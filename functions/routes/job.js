const express = require('express');
const router = express.Router();

const Auth = require('../utils/auth');
const Job = require('../controllers/job');

router.get('/', Auth, Job.List);
router.post('/', Auth, Job.Create);
router.get('/:jobId', Auth, Job.Retrieve);
router.put('/:jobId', Auth, Job.Update);
router.delete('/:jobId', Auth, Job.Delete);

module.exports = router;