const express = require('express');
const router = express.Router();

const Auth = require('../utils/auth');
const { Project, Task } = require('../controllers/project');

router.get('/', Auth, Project.List);
router.post('/', Auth, Project.Create);
router.get('/:projectId', Auth, Project.Retrieve);
router.put('/:projectId', Auth, Project.Update);
router.delete('/:projectId', Auth, Project.Delete);
router.post('/:projectId/task', Auth, Project.CreateTask);
router.put('/:projectId/task/:taskId', Auth, Task.Update);
router.delete('/:projectId/task/:taskId', Auth, Task.Delete);

module.exports = router;