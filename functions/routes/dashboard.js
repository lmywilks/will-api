const express = require('express');
const router = express.Router();
const Auth = require('../utils/auth');
const Dashboard = require('../controllers/dashboard');

router.get('/', Auth, Dashboard.Init);

module.exports = router;