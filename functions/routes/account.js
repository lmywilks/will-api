const express = require('express');
const router = express.Router();

const Auth = require('../utils/auth');
const Account = require('../controllers/account');

router.get('/', Auth, Account.List);
router.post('/', Auth, Account.Create);
router.get('/:accountId', Auth, Account.Retrieve);
router.put('/:accountId', Auth, Account.Update);
router.delete('/:accountId', Auth, Account.Delete);

module.exports = router;