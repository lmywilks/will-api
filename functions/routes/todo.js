const express = require('express');
const router = express.Router();

const Auth = require('../utils/auth');
const Todo = require('../controllers/todo');

router.get('/', Auth, Todo.List);
router.post('/', Auth, Todo.Create);
router.get('/:groupId', Auth, Todo.Retrieve);
router.put('/:groupId', Auth, Todo.Update);
router.put('/:groupId/list', Auth, Todo.UpdateList);
router.delete('/:groupId', Auth, Todo.Delete);

module.exports = router;