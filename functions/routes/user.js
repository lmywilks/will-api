const express = require('express');
const router = express.Router();
const Auth = require('../utils/auth');
const User = require('../controllers/user');

router.get('/', Auth, User.GetAuthenticateUser);
router.get('/:username', User.GetUserDetails);
router.post('/', Auth, User.AddUserDetails);
router.post('/image', Auth, User.UploadImage);

module.exports = router;