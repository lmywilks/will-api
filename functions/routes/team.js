const express = require('express');
const router = express.Router();

const Auth = require('../utils/auth');
const Team = require('../controllers/team');

router.get('/', Auth, Team.List);
router.post('/', Auth, Team.Create);
router.get('/:teamId', Auth, Team.Retrieve);
router.put('/:teamId', Auth, Team.Update);
router.delete('/:teamId', Auth, Team.Delete);

module.exports = router;