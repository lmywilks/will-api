const express = require('express');
const router = express.Router();

const Auth = require('../controllers/auth');

router.get('/', (req, res, next) => {
    res.json({ title: 'Will Play' });
});

router.post('/login', Auth.Login);
router.post('/signup', Auth.Signup);

module.exports = router;