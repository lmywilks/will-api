const functions = require('firebase-functions');
const express = require('express');
const cors = require('cors');

const app = express();

const indexRoute = require('./routes/index');
const userRoute = require('./routes/user');
const accountRoute = require('./routes/account');
const dashboardRoute = require('./routes/dashboard');
const jobRoute = require('./routes/job');
const todoRoute = require('./routes/todo');
const teamRoute = require('./routes/team');
const projectRoute = require('./routes/project');

const Helper = require('./utils/helper');

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cors({ origin: true }));

app.use(function(req, res, next) {
    //set headers to allow cross origin request.
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use('/', indexRoute);
app.use('/user', userRoute);
app.use('/account', accountRoute);
app.use('/dashboard', dashboardRoute);
app.use('/job', jobRoute);
app.use('/todo', todoRoute);
app.use('/team', teamRoute);
app.use('/project', projectRoute);

exports.api = functions
    .region('us-east1')
    .https
    .onRequest(app);

exports.onUserDeleted = functions
    .region('us-east1')
    .firestore
    .document('/users/{username}')
    .onDelete(Helper.UserDelete);

exports.onRestaurantDeleted = functions
    .region('us-east1')
    .firestore
    .document('/restaurants/{restaurantId}')
    .onDelete(Helper.RestaurantDelete);

exports.onTeamDeleted = functions
    .region('us-east1')
    .firestore
    .document('/teams/{teamId}')
    .onDelete(Helper.TeamDelete);

exports.onProjectDeleted = functions
    .region('us-east1')
    .firestore
    .document('/projects/{projectId}')
    .onDelete(Helper.ProjectDelete);
