const { db } = require('../utils/admin');
const Base = require('./base');

const Project = {

    List: async (req, res) => {
        try {
            let result = {
                projects: [],
                teams: []
            };

            const projects = await db
                .collection('projects')
                .where('user', '==', req.user.username)
                .get();

            projects.forEach(doc => {
                if (doc.exists) {
                    const data = doc.data();
                    data.projectId = doc.id
                    result.projects.push(data);
                }
            });

            const teams = await db
                .collection('teams')
                .where('user', '==', req.user.username)
                .get();

            teams.forEach(doc => {
                if (doc.exists) {
                    const data = doc.data();
                    data.teamId = doc.id;
                    result.teams.push(data);
                }
            });

            return res.json(result);
        } catch (err) {
            console.log(err)
        }
    },

    Create: async (req, res) => {
        return Base.Create(req, res, 'projects', 'reduceProjectDetails', 'validateProjectCreate', 'projectId');
    },

    Retrieve: async (req, res) => {
        try {
            const doc = await db
                .doc(`/projects/${ req.params.projectId }`)
                .get();

            if (!doc.exists) return res.status(404).json({ error: 'Doc not found.' });

            const user = await db
                .doc(`/users/${ doc.data().user }`)
                .get();

            if (!user.exists) return res.status(401).json({ error: 'Unauthorized.' });

            let result = doc.data();
            result.projectId = doc.id;
            result.owner = {
                name: (user.data().details.firstName || user.data().details.lastName) ? (user.data().details.firstName + ' ' + user.data().details.lastName) : user.data().username,
                userId: user.id
            };

            result.tasks = await Task.List(req.params.projectId);

            return res.json(result);

        } catch (err) {
            console.log(err)
        }
        return Base.Retrieve(req, res, 'projects', req.params.projectId, 'projectId');
    },

    Update: async (req, res) => {
        return Base.Update(req, res, 'projects', req.params.projectId, ['projectId']);
    },

    Delete: async (req, res) => {
        return Base.Delete(req, res, 'projects', req.params.projectId);
    },

    CreateTask: async (req, res) => {
        try {

            const doc = await db
                .doc(`/projects/${ req.params.projectId }`)
                .get();

            if (!doc.exists) return res.status(404).json({ error: 'Doc not found.' });

            const user = await db
                .doc(`/users/${ doc.data().user }`)
                .get();

            if (!user.exists) return res.status(401).json({ error: 'Unauthorized.' });

            const task = await Task.Create(req, res);

            let data = doc.data();

            if (!data.active) {
                data.active = true;
            }

            if (!data.checkIn || data.checkIn === '') {
                data.checkIn = new Date().toISOString();
            }

            data.updatedAt = new Date().toISOString();

            return res.json(task);

        } catch (err) {
            console.log(err);
        }
    }

};

const Task = {
    List: async (projectId) => {
        try {
            let result = [];

            const tasks = await db
                .collection('tasks')
                .where('projectId', '==', projectId)
                .get();

            tasks.forEach(doc => {
                if (doc.exists) {
                    const data = doc.data();
                    data.taskId = doc.id
                    result.push(data);
                }
            });

            return result;
        } catch (err) {
            console.log(err);
            return [];
        }
    },

    Create: async (req, res) => {
        try {

            let newTask = req.body;

            if (!newTask.status) {
                newTask.status = 'Open';
            }

            newTask.projectId = req.params.projectId;
            newTask.createdAt = new Date().toISOString();
            newTask.updatedAt = new Date().toISOString();

            const doc = await db
                .collection('tasks')
                .add(newTask);

            const result = newTask;
            result.taskId = doc.id;

            return result;
        } catch (err) {
            console.error(err);
        }
    },

    Update: async (req, res) => {
        try {
            const document = db.doc(`/tasks/${ req.params.taskId }`);

            const doc = await document.get();

            if (!doc.exists) return res.status(404).json({ error: 'Doc not found.' });

            if (doc.data().projectId !== req.params.projectId) return res.status(404).json({ error: 'Project not found.' });

            let data = doc.data();

            data.name = req.body.name || data.name;
            data.status = req.body.status || data.status;
            data.description = req.body.description || data.description;
            data.updatedAt = new Date().toISOString();

            await document.update(data);

            return res.json({ message: 'Updated successfully.' });
        } catch (err) {
            console.log(err);
        }
    },

    Delete: async (req, res) => {
        try {
            const document = db.doc(`/tasks/${ req.params.taskId }`);

            const doc = await document.get();

            if (!doc.exists) return res.status(404).json({ error: 'Doc not found.' });

            if (doc.data().projectId !== req.params.projectId) return res.status(404).json({ error: 'Project not found.' });

            await document.delete();

            return res.json({ message: 'Object deleted successfully.' });
        } catch (err) {
            console.log(err)
        }
    }
};

module.exports = { Project, Task };