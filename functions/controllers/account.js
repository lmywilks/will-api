const { db } = require('../utils/admin');
const Base = require('./base');

const Account = {

    List: async (req, res) => {
        try {
            let result = {
                accounts: [],
                jobs: []
            };

            const accounts = await db
                .collection('accounts')
                .where('user', '==', req.user.username)
                .orderBy('createdAt', 'desc')
                .get();

            accounts.forEach(doc => {
                if (doc.exists) {
                    const data = doc.data();
                    data.accountId = doc.id;
                    result.accounts.push(data);
                }
            });

            const jobs = await db
                .collection('jobs')
                .where('user', '==', req.user.username)
                .orderBy('createdAt', 'desc')
                .get();

            jobs.forEach(doc => {
                if (doc.exists) {
                    const data = doc.data();
                    data.jobId = doc.id;
                    result.jobs.push(data);
                }
            });

            return res.json(result);
        } catch (err) {
            console.log(err)
        }
    },

    Create: async (req, res) => {
        return Base.Create(req, res, 'accounts', 'reduceAccountDetails', 'validateAccountCreate', 'accountId');
    },

    Retrieve: async (req, res) => {
        return Base.Retrieve(req, res, 'accounts', req.params.accountId, 'accountId');
    },

    Update: async (req, res) => {
        return Base.Update(req, res, 'accounts', req.params.accountId, ['accountId']);
    },

    Delete: async (req, res) => {
        return Base.Delete(req, res, 'accounts', req.params.accountId);
    }

}

module.exports = Account;