const Base = require('./base');

const Team = {

    List: async (req, res) => {
        return Base.List(req, res, 'teams', 'createdAt', 'teamId');
    },

    Create: async (req, res) => {
        if (req.body) {
            if (!req.body.member || !req.body.member.length) {
                req.body.member = [
                    {
                        imageUrl: req.user.imageUrl,
                        userId: req.user.userId,
                        name: (
                            req.user.details && 
                            (
                                req.user.details.firstName || 
                                req.user.details.lastName
                            )) ? (req.user.details.firstName + ' ' + req.user.details.lastName) : req.user.username
                    }
                ];
            }
        } 

        return Base.Create(req, res, 'teams', null, 'validateTeamCreate', 'teamId');
    },

    Retrieve: async (req, res) => {
        return Base.Retrieve(req, res, 'teams', req.params.teamId, 'teamId');
    },

    Update: async (req, res) => {
        return Base.Update(req, res, 'teams', req.params.teamId, ['teamId']);
    },

    Delete: async (req, res) => {
        return Base.Delete(req, res, 'teams', req.params.teamId);
    }

};

module.exports = Team;