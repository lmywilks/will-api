const { db } = require('../utils/admin');

const Dashboard = {

    Init: async (req, res) => {
        try {
            const now = new Date();

            let result = {
                expense: {
                    daily: 0,
                    monthly: 0,
                    yearly: 0
                },
                income: {
                    monthly: 0,
                    yearly: 0
                },
                accounts: [],
                todo: {}
            };
            
            const accounts = await db
                .collection('accounts')
                .where('user', '==', req.user.username)
                .orderBy('createdAt', 'desc')
                .get();

            accounts.forEach((doc) => {
                if (doc.exists) {
                    if (result.accounts.length < 5) {
                        const data = doc.data();
                        data.accountId = doc.id;
                        result.accounts.push(data);
                    }
                    switch (doc.data().type) {
                        case 'Expense':
                            if (doc.data().date.year === now.getFullYear()) {
                                result.expense.yearly += doc.data().money;
                            }
                            if (doc.data().date.month === now.getMonth() + 1) {
                                result.expense.monthly += doc.data().money;
                            }
                            if (doc.data().date.day === now.getDate()) {
                                result.expense.daily += doc.data().money;
                            }
                            break;
                        case 'Income':
                            if (doc.data().date.year === now.getFullYear()) {
                                result.income.yearly += doc.data().money;
                            }
                            if (doc.data().date.month === now.getMonth() + 1) {
                                result.income.monthly += doc.data().money;
                            }
                            break;
                        default:
                            break;
                    }
                }
            });

            const todo = await db
                .collection('todo')
                .where('user', '==', req.user.username)
                .orderBy('createdAt', 'desc')
                .limit(1)
                .get();
            
            todo.forEach(doc => {
                if (doc.exists) {
                    const data = doc.data();
                    data.groupId = doc.id;
                    result.todo = data;
                }
            });

            return res.json(result);
            
        } catch (err) {
            console.log(err);
            return res.status(500).json({ error: err.code });
        }
    }

};


module.exports = Dashboard;