const { db } = require('../utils/admin');
const Validator = require('../utils/validators');

const Base = {
    
    List: async (req, res, collection, orderBy, id) => {
        try {
            let results = [];

            const data = await db
                .collection(collection)
                .orderBy(orderBy, 'desc')
                .get();

            data.forEach(doc => {
                results.push({
                    [id]: doc.id,
                    ...doc.data()
                });
            });

            return res.json(results);
        } catch (err) {
            console.error(err);
        }
    },

    Create: async (req, res, collection, reducer, validator, id) => {
        try {

            let detail;

            if (reducer) {
                detail = Validator[reducer](req.body);
            } else {
                detail = req.body;
            }

            if (validator) {
                const { errors, valid } = Validator[validator](detail);

                if (!valid) return res.status(400).json(errors);
            }
            
            const newObj = {
                ...detail,
                createdAt: new Date().toISOString(),
                updatedAt: new Date().toISOString(),
                user: req.user.username 
            };

            const doc = await db
                .collection(collection)
                .add(newObj);

            const result = newObj;
            result[id] = doc.id;

            return res.json(result);
        } catch (err) {
            console.error(err);
        }
    },

    Retrieve: async (req, res, collection, id, obj_id) => {
        try {
            const doc = await db
                .doc(`/${ collection }/${ id }`)
                .get();

            if (!doc.exists) return res.status(404).json({ error: 'Doc not found.'});

            let result = doc.data();
            result[obj_id] = doc.id;

            return res.json(result);
        } catch (err) {
            console.log(err);
        }
    },

    Update: async (req, res, collection, id, blackList) => {
        try {
            const document = db.doc(`/${ collection }/${ id }`);

            const doc = await document.get();

            if (!doc.exists) return res.status(404).json({ error: 'Doc not found.' });

            if (doc.data().user !== req.user.username) return res.status(401).json({ error: 'Unauthorized' });

            let data = doc.data();

            for(let i in req.body) {
                if (
                    i != 'createdAt' && 
                    i != 'user' && 
                    i != 'updatedAt' && 
                    blackList.indexOf(i) === -1
                ) {
                    data[i] = req.body[i];
                }
            }

            data.updatedAt = new Date().toISOString();

            await document.update(data);

            return res.json({ message: 'Updated successfully.' });

        } catch (err) {
            console.log(err);
        }
    },

    Delete: async (req, res, collection, id) => {
        try {
            const document = db.doc(`/${ collection }/${ id }`);

            const doc = await document.get();

            if (!doc.exists) return res.status(404).json({ error: 'Doc not found.' });

            if (doc.data().user !== req.user.username) return res.status(401).json({ error: 'Unauthorized.' });

            await document.delete();

            return res.json({ message: 'Object deleted successfully.' });
        } catch (err) {
            console.log(err);
        }
    }

};

module.exports = Base;