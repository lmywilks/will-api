const { db } = require('../utils/admin');
const { FIREBASE } = require('../utils/config');

const Validator = require('../utils/validators');

const firebase = require('firebase');
firebase.initializeApp(FIREBASE);

const Auth = {

    Login: async (req, res) => {
        try {
            const login = { ...req.body };
    
            const { errors, valid } = Validator.validateLoginData(login);
    
            if (!valid) return res.status(400).json(errors);
    
            const data = await firebase
                .auth()
                .signInWithEmailAndPassword(login.email, login.password);
    
            const token = await data.user.getIdToken();
    
            return res.json({ token });
        } catch (err) {
            console.log(err);
            // auth/wrong-password
            // auth/user-not-found
            return res
                .status(403)
                .json( { general: 'Wrong credentials, please try again.' });
        }
    },

    Signup: async (req, res) => {
        try {
            const newUser = { ...req.body };
    
            const { errors, valid } = Validator.validateSignupData(newUser);
    
            if (!valid) return res.status(400).json(errors);
        
            const noImg = 'no-img.png';
    
            const doc = await db
                .doc(`/users/${ newUser.username }`)
                .get();
            
            if (doc.exists) {
                return res
                    .status(400)
                    .json({ username: 'this username is already exist'});
            }
    
            const data = await firebase
                .auth()
                .createUserWithEmailAndPassword(newUser.email, newUser.password);
    
            const userId = data.user.uid;
            const token = await data.user.getIdToken();
    
            const userCredentials = {
                username  : newUser.username,
                email     : newUser.email,
                createdAt : new Date().toISOString(),
                updatedAt : new Date().toISOString(),
                role      : 'user',
                imageUrl  : `https://firebasestorage.googleapis.com/v0/b/${ FIREBASE.storageBucket }/o/${ noImg }?alt=media`,
                userId
            };
    
            await db
                .doc(`/users/${ newUser.username }`)
                .set(userCredentials);
    
            return res.status(201).json({ token });
        } catch (err) {
            console.log(err);
            if (err.code === 'auth/email-already-in-use') {
                return res.status(400).json({ email: 'Email is already in use.' });
            } else if (err.code === 'auth/weak-password') {
                return res.status(400).json({ password: 'Password should be at least 6 characters.' });
            } else {
                return res.status(500).json({ general: 'Something went wrong, please try again.' });
            }
        }
    }

};

module.exports = Auth;
