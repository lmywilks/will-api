const Base = require('./base');

const Job = {

    List: async (req, res) => {
        return Base.List(req, res, 'jobs', 'createdAt', 'jobId');
    },

    Create: async (req, res) => {
        return Base.Create(req, res, 'jobs', 'reduceJobDetails', 'validateJobCreate', 'jobId');
    },

    Retrieve: async (req, res) => {
        return Base.Retrieve(req, res, 'jobs', req.params.jobId, 'jobId');
    },

    Update: async (req, res) => {
        return Base.Update(req, res, 'jobs', req.params.jobId, ['jobId']);
    },

    Delete: async (req, res) => {
        return Base.Delete(req, res, 'jobs', req.params.jobId);
    }

};

module.exports = Job;