const { db } = require('../utils/admin');
const Base = require('./base');

const Todo = {
    
    List: async (req, res) => {
        return Base.List(req, res, 'todo', 'createdAt', 'groupId');
    },

    Create: async (req, res) => {
        return Base.Create(req, res, 'todo', 'reduceTodoDetails', 'validateTodoCreate', 'groupId');
    },

    Retrieve: async (req, res) => {
        return Base.Retrieve(req, res, 'todo', req.params.groupId, 'groupId');
    },

    Update: async (req, res) => {
        return Base.Update(req, res, 'todo', req.params.groupId, ['groupId', 'list']);
    },

    Delete: async (req, res) => {
        return Base.Delete(req, res, 'todo', req.params.groupId);
    },

    UpdateList: async (req, res) => {
        try {
            const document = db.doc(`todo/${ req.params.groupId }`);

            const doc = await document.get();

            if (!doc.exists) return res.status(404).json({ error: 'Doc not found.' });

            let data = doc.data();

            data.list = req.body;
            data.updatedAt = new Date().toISOString();

            if (data.list && data.list.length) {
                data.done = data.list.filter(x => !x.done).length === 0 ? true : false;
            }

            await document.update(data);

            return res.json({ message: 'Updated successfully.' });
        } catch (err) {
            console.error(err);
        }
    }
};

module.exports = Todo;