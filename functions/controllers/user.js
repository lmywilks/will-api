
const { admin, db } = require('../utils/admin');
const { FIREBASE_BUCKET_ROOT } = require('../utils/config');

const Validator = require('../utils/validators');

const User = {

    AddUserDetails: async (req, res) => {
        try {
            const userDetails = Validator.reduceUserDetails(req.body);
    
            await db
                .doc(`/users/${ req.user.username }`)
                .update({ details: userDetails})
    
            return res.json({ message: 'Details added successfully.' });
    
        } catch (err) {
            console.error(err);
            return res.status(500).json({ error: err.code });
        }
    },

    GetUserDetails: async (req, res) => {
        try {
            let userData = {};
    
            const doc = await db
                .doc(`/users/${ req.params.username }`)
                .get();
    
            if (!doc.exists) return res.status(404).json({ error: 'User not found.'});
    
            userData.user = doc.data();
    
            return res.json(userData);
        } catch (err) {
            console.error(err);
            return res.status(500).json({ error: err.code });
        }
    },

    GetAuthenticateUser: async (req, res) => {
        try {
            let userData = {};
    
            const doc = await db
                .doc(`/users/${ req.user.username }`)
                .get();
    
            if(!doc.exists) return res.status(401).json({ message: 'Unauthorized.' });
    
            userData.credentials = doc.data();
    
            const notifications = await db
                .collection('notifications')
                .where('recipient', '==', req.user.username)
                .orderBy('createdAt', 'desc')
                .limit(10)
                .get();
    
            notifications.forEach(doc => {
                userData.notifications.push({
                    ...doc.data(),
                    notificationId: doc.id
                });
            });
    
            return res.json(userData);
        } catch (err) {
            console.error(err);
            return res.status(500).json({ error: err.code });
        }
    },

    MarkNotificationsRead: async (req, res) => {
        try {
            let batch = db.batch();
    
            req.body.forEach(notificationId => {
                const notification = db.doc(`/notifications/${ notificationId }`);
                batch.update(notification, { read: true });
            });
    
            await batch.commit();
    
            return res.json({ message: 'Notifications marked read.'});
        } catch (err) {
            console.error(err);
            return res.status(500).json({ error: err.code });
        }
    },

    UploadImage: async (req, res) => {
        try {
            const BusBoy = require('busboy');
            const path = require('path');
            const os = require('os');
            const fs = require('fs');

            let imageFileName;
            let imageToBeUploaded = {};
            let imageUrl;
        
            const busboy = new BusBoy({ headers: req.headers });

            busboy.on('file', (fieldname, file, filename, encoding, mimetype) => {
                if (
                    mimetype !== 'image/jpeg' && 
                    mimetype !== 'image/png' && 
                    mimetype !== 'image/jpg'
                ) {
                    return res.status(400).json({ error: 'wrong file type submitted.' });
                }
                
                const imageExtension = filename.split('.')[filename.split('.').length - 1];
                imageFileName = `${ req.user.username }-${ new Date().getTime() }.${ imageExtension }`;
                const filepath = path.join(os.tmpdir(), imageFileName);
        
                imageToBeUploaded = { filepath, mimetype };
        
                file.pipe(fs.createWriteStream(filepath));
            });

            busboy.on('finish', () => {
                admin
                    .storage()
                    .bucket()
                    .upload(
                        imageToBeUploaded.filepath, 
                        { 
                            destination: `profile/${ req.user.username }/${ imageFileName }`,
                            resumable: false, 
                            metadata: { 
                                metadata: {
                                    contentType: imageToBeUploaded.mimetype 
                                }
                            }
                        }
                    )
                    .then(() => {
                        imageUrl = `${ FIREBASE_BUCKET_ROOT }/profile%2F${ req.user.username }%2F${ imageFileName }?alt=media`;
                        return db
                            .doc(`/users/${ req.user.username }`)
                            .update({ imageUrl });
                    })
                    .then(() => {
                        return res.json({ url: imageUrl });
                    })
                    .catch(err => {
                        console.error(err);
                        return res.status(500).json({ error: err.code });
                    });
            });
        
            busboy.end(req.rawBody);

        } catch (err) {
            console.log(error);
            return res.status(500).json({ error: err.code });
        }
    }

}

module.exports = User;
